# ASCII CAM

**ASCII CAM**

* O que é?
  * é um pequeno projeto que converte o input da sua webcam e mostra no console em forma de texto.
  * converte cada frame em ascii e imprime no console
  * caso queira usar 'cores' invertidas descomente a segunda linha de asciify.py

![Example](/example.png)