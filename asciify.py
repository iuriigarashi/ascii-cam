ASCII_CHARS = [' ', '.', ',', ':', ';', '+', '*', '?', '%', '#', '@']
# ASCII_CHARS = ['@', '#', '%', '/', '*', '+', ';', ':', ',', '.', ' '] # inverted colors


def resize_image(image, new_width=100):
    width, height = image.size
    ratio = height / width
    new_heigth = int(new_width * ratio)
    # print(str(ratio) + '-' + str(width) + '-' + str(height) + '|' + str(new_width) + '-' + str(new_heigth))
    resized_image = image.resize((new_width, new_heigth))
    return resized_image


def grayify(image):
    grayscale_image = image.convert("L")
    return grayscale_image


def pixels_to_ascii(image):
    pixels = image.getdata()
    characters = ''.join([ASCII_CHARS[pixel//25] for pixel in pixels])
    return characters
