import cv2 as cv
import numpy as np
from PIL import Image
from asciify import grayify, pixels_to_ascii, resize_image
import os


def clear(): return os.system('cls')


cap = cv.VideoCapture(0, cv.CAP_DSHOW)
new_width = 100

num = 1
save_output = False

if not cap.isOpened():
    print("Cannot open camera")
    exit()
while True:
    # capture frame-by-frame
    ret, frame = cap.read()
    # if frame is read correctly ret is True
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break
    # mathemagics
    img = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
    im_pil = Image.fromarray(img)

    img_gray = grayify(resize_image(im_pil))

    ascii_image_pixels = pixels_to_ascii(img_gray)
    pixel_count = len(ascii_image_pixels)
    ascii_image = "\n".join(ascii_image_pixels[i:(
        i+new_width)] for i in range(0, pixel_count, new_width))

    # clear console
    clear()
    # Show the "image" in console
    print(ascii_image)

    
    if save_output == True:
        num = num+1
        if num % 60 == 0:
            with open("output_ascii/ascii_img" + "_" + str(num) + ".txt", "w") as f:
                f.write(ascii_image)

    # Display the frame
    # cv.imshow('frame', img)

# close the capture
cap.release()
cv.destroyAllWindows()
